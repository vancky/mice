function [ states ] = getEpheEzr( naifIds,et,frame,abcorr,observe )
% get position to calculate the potential
% input
%      naifIds ,1*N,number,object to get position see also NAIF ID, or cell inputs
%      et , ephemeris time
%      frame,
%      abcorr
%      observe ,also read mice_spkpos
% output
%       states,6*N ,every column is an object ,state (position and velocity) of
%  a target body relative to an observing body
% first version by lifan@pmo.ac.cn
% 2015/9/8
% update , 2018/6/18 for time improve,now naifIds support cell inputs
% update ,2020/7/2 for speed up ,to call the mice directly
states=zeros(6,length(naifIds));
if isnumeric(naifIds(1))
    for k=1:length(naifIds)
        % states(:,k)=cspice_spkezr(int2str(naifIds(k)),et,frame,abcorr,observe);
        starg = mice('spkezr_s',int2str(naifIds(k)),et,frame,abcorr,observe);
        states(:,k)=starg.state;
    end
else
    for k=1:length(naifIds)
        starg = mice('spkezr_s',naifIds{k},et,frame,abcorr,observe);
        states(:,k)=starg.state;
        % states(:,k)=cspice_spkezr(naifIds{k},et,frame,abcorr,observe);
    end
end


