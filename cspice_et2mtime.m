function mTime= cspice_et2mtime(et)
%transform from et to matlab date time (+8)
%  input: et et time scale of spice
%  ouput: datetime object in matlab timezone is utc+8 beijing time
%  first version at 2020/10/13 by lifan@pmo.ac.cn
epochJ2000=datetime('2000-01-01 11:58:50.82','TimeZone','UTC');
mTime=datetime(et,'ConvertFrom','epochtime','Epoch',epochJ2000)+hours(8);
mTime.TimeZone='UTC+8';
end

