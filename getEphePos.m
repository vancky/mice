function [ states ] = getEphePos( naifIds,et,frame,abcorr,observe )
% get position to calculate the potential
% input 
%      naifIds ,1*N,number,object to get position see also NAIF ID
%      et , ephemeris time 
%      frame, 
%      abcorr
%      observe ,also read mice_spkpos 
% output
%       states,3*N ,every column is an object
% first version by lifan@pmo.ac.cn
% 2015/8/21
% update , 2018/6/18 for time improve,now naifIds support cell inputs
states=zeros(3,length(naifIds));
if isnumeric(naifIds(1))
    for k=1:length(naifIds)
        states(:,k)=cspice_spkpos(int2str(naifIds(k)),et,frame,abcorr,observe);
    end
else
    for k=1:length(naifIds)
        states(:,k)=cspice_spkpos(naifIds{k},et,frame,abcorr,observe);
    end
end

